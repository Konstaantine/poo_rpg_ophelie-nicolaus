"use strict";
exports.__esModule = true;
// Crée une application TypeScript orientée objets en ligne de commande de combat de héros en arène.
var Hero_1 = require("./classes/Hero");
var hero1 = new Hero_1["default"]('Mathilde', 50, 100);
var hero2 = new Hero_1["default"]('Marie', 60, 100);
hero1.isAlive();
console.log(hero1.life);
console.log(hero2.name);
console.log('coucou');
