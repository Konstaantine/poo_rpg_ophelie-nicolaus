"use strict";
// Partie 1 : Héros
// ​
exports.__esModule = true;
var Hero = /** @class */ (function () {
    function Hero(name, power, life) {
        this.name = name;
        this.power = power;
        this.life = life;
    }
    Hero.prototype.attack = function (opponent) {
        this.life = this.life -= opponent.power;
    };
    ;
    Hero.prototype.isAlive = function () {
        if (this.power > 0) {
            console.log('Vous êtes vivant-e !');
            return true;
        }
        console.log('Vous êtes mort-e !');
        return false;
    };
    ;
    return Hero;
}());
exports["default"] = Hero;
