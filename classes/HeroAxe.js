"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Hero_1 = require("./Hero");
var HeroAxe = /** @class */ (function (_super) {
    __extends(HeroAxe, _super);
    function HeroAxe(power, life) {
        var _this = _super.call(this, power, life, 'axe') || this;
        _this.name = 'axe';
        return _this;
    }
    HeroAxe.prototype.attack = function (opponent) {
        if (opponent.name == 'sword') {
            opponent.power = opponent.power * 2;
        }
    };
    return HeroAxe;
}(Hero_1["default"]));
exports["default"] = HeroAxe;
// `HeroAxe` : si le type de `opponent` est `HeroSword`, multiplier `power` par deux
