// Partie 1 : Héros
// ​

// La classe `Hero` permet de créer des objets possédant les propriétés suivantes :

// name : string
// power : number
// life : number
// ​

// Et les méthodes

// attack(opponent: Hero)
// isAlive()
// ​

// La méthode `attack` a un paramètre `opponent` (de type `Hero`). Il faut réduire le nombre (`life`) 
//de `opponent` d'autant de dégats (`power`) de l'attaquant.


// Exemple : Si Joan attaque Leon, cela sera représenté par :

// joan.attack(leon)
// ​

// La méthode `isAlive` devrait retourner `true` si le nombre de points de vie du héros est supérieur à zéro et `false` sinon.


// Crée deux instances de `Hero` et vérifie que les méthodes `attack` et `isAlive` fonctionnent.


import Weapon from "./Weapon";

export default class Hero {

    public name:string;
    public power:number;
    public life:number;
    public weapon:Weapon;
   
    constructor(name:string, power:number, life:number) {
        this.name = name;
        this.power = power;
        this.life = life;
        
    }

    attack (opponent:Hero){
        console.log('Le combat début entre ' + this.name + " et " + opponent.name + '. Préparez-vous au combat, héros et héroïnes du jour ! ')
        opponent.life -= (this.power + this.weapon.damage);
        console.log(opponent.name + " n'a plus que " + opponent.life + " PV" + " " + this.name + " détient encore " + this.life + "PV."   )
    };

    isAlive (){
        if (this.life > 0) {
        console.log(this.name + ' est vivant-e !')
        return true
                }
        console.log(this.name + ' est mort-e !')

        return false
            };

}



