// Partie 3 : Bataille
// Crée une boucle qui fait que deux instances de sous-classes `Hero` s'affrontent (elles attaquent en même temps).
// Quand au moins l'une d'entre elles est morte, afficher `{heroName} wins`. Si les deux sont morts, afficher `It's a draw`.


// Bonus 1 : Les dégâts de l'arme

// Ajoute une propriété damage à la classe Weapon et fait en sorte qu'elle soit initialisée par le constructeur.


// Modifie la méthode attack de Hero afin que les dégâts soient calculés de la façon suivante : 
//la puissance du héro power + les dégâts de l'arme power


// Bonus 2 : Interface graphique

// Réalise une interface graphique pour l'application (par exemple, avec un choix de héros et d'armes, et un visuel sur les dégâts infligés)

import Hero from './Hero';
import Weapon from './Weapon';

export default class Battle {
    public player1:Hero;
    public player2:Hero; 
        constructor(player1:Hero, player2:Hero) {
            this.player1 = player1,
            this.player2 = player2
        }


        battle(player1:Hero, player2:Hero) {
            while ((player1.life > 0) && (player2.life > 0)) {
                player1.life -= (player2.power + player2.weapon.damage);
                player2.life -= (player1.power + player1.weapon.damage);
           
            if ((player1.life <= 0) && (player2.life > 0)) {
                            console.log(player2.name + " wins with " + player2.life + " PV. RIP opponent !" )
                        }else if ((player2.life <= 0) && (player1.life > 0)) {
                            console.log(player1.name + " wins with " + player1.life + " PV. RIP opponent !" )
                        }else if ((player1.life <= 0 ) && (player2.life <= 0)) {
                                   console.log("It's a draw !")
                            }
        }
    }
    }


    