"use strict";
// Partie 2 : Armes
exports.__esModule = true;
// Crée une classe `Weapon` avec la propriété suivante :
// name : string
// Ajoute l'attribut `weapon` (de type `Weapon`) à la classe `Hero` sans modifier le constructeur 
//(ainsi `weapon` n'est pas initialisé).
// Crée trois classes `HeroAxe`, `HeroSword` et `HeroSpear` qui héritent de `Hero`.
// Ces trois classes appellent le constructeur de leur parent et initialisent `weapon` avec des instances de la classe `Weapon` 
//dont les noms seront `axe`, `sword` ou `spear` selon le cas.
// Dans les classes `HeroAxe`, `HeroSword` et `HeroSpear`, redéfinisse la méthode `attack` pour prendre en compte les cas suivants :
// `HeroAxe` : si le type de `opponent` est `HeroSword`, multiplier `power` par deux
// `HeroSword` : si le type de `opponent` est `HeroSpear`, multiplier `power` par deux
// `HeroSpear` : si le type de `opponent` est `HeroAxe`, multiplier `power` par deux
// Astuce : utilise le mot-clé `super` pour appeler la méthode `attack` de la classe parente.
// ​
// Crée des instances des trois classes `HeroAxe`, `HeroSword` et `HeroSpear` et vérifie que leurs méthodes `attack` fonctionnent correctement.
var Weapon = /** @class */ (function () {
    function Weapon(name) {
        name = this.name;
    }
    return Weapon;
}());
exports["default"] = Weapon;
