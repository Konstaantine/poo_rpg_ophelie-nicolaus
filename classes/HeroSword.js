"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Hero_1 = require("./Hero");
var HeroSword = /** @class */ (function (_super) {
    __extends(HeroSword, _super);
    function HeroSword(power, life) {
        var _this = _super.call(this, power, life, 'sword') || this;
        _this.name = 'sword';
        return _this;
    }
    HeroSword.prototype.attack = function (opponent) {
        if (opponent.weapon.name == 'spear') {
            opponent.power = opponent.power * 2;
        }
    };
    return HeroSword;
}(Hero_1["default"]));
exports["default"] = HeroSword;
// `HeroSword` : si le type de `opponent` est `HeroSpear`, multiplier `power` par deux
