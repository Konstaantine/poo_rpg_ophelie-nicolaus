import Hero from "./Hero";
import HeroAxe from "./HeroAxe";
import Weapon from "./Weapon";



export default class HeroSpear extends Hero {
    constructor(name:string, power:number, life:number, weapon:Weapon) {
        super(name, power, life);
        this.weapon = weapon;
    
    }

    attack(opponent:Hero) {
        console.log('Le combat début entre ' + this.name + " et " + opponent.name + '. Préparez-vous au combat, héros et héroïnes du jour !')
        if (opponent.weapon.name == 'axe') {
            opponent.power = opponent.power * 2;
            opponent.life -= this.power;
            console.log(opponent.name + " n'a plus que " + opponent.life + " PV" + " " + this.name + " détient encore " + this.life + "PV."   )

        }else {
            opponent.life -= this.power;
            console.log(opponent.name + " n'a plus que " + opponent.life + " PV" + " " + this.name + " détient encore " + this.life + "PV."   )

        }
    }

}

// `HeroSpear` : si le type de `opponent` est `HeroAxe`, multiplier `power` par deux
