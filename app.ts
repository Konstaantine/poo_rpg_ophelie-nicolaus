// Crée une application TypeScript orientée objets en ligne de commande de combat de héros en arène.
import Hero  from './classes/Hero';
import HeroAxe  from './classes/HeroAxe';
import HeroSpear  from './classes/HeroSpear';
import HeroSword  from './classes/HeroSword';
import Battle from './classes/Battle';
import Weapon from './classes/Weapon';



// NEW HEROS
let Bob = new HeroSpear('Bob', 3, 5, new Weapon('spear', 5))
let Karina = new HeroAxe('Karina', 150, 40, new Weapon('axe', 50))
let Ernesto = new HeroSword('Ernesto', 20, 78, new Weapon('sword', 10))
let Jamiroquai = new HeroSpear('Jamiroquai', 10, 10, new Weapon('spear', 10))

// NEW BATTLE
let fight1 = new Battle(Ernesto, Karina)
let fight2 = new Battle (Bob, Karina)
let fight3 = new Battle (Jamiroquai, Bob)
let fight4 = new Battle (Ernesto, Bob)


// PLAY BATTLE
fight3.battle(Karina, Ernesto)